const { Usuarios } = require('../classes/usuarios');
const { io } = require('../server');
const { crearMensaje}= require('../utils/utils');



const usuarios = new Usuarios();
io.on('connection', (client) => {

client.on('entraChat',(data, callback)=>{
    if( !data.nombre || !data.sala ){
        return callback({
            error: true,
            mensaje: 'El nombre es necesario'
        })
    }


    
    client.join(data.sala)
    usuarios.agregarPersonas(client.id, data.nombre, data.sala);
    client.broadcast.to(data.sala).emit('crearMensaje',crearMensaje('Administrador',`${data.nombre} entró`))
    
   client.broadcast.to(data.sala).emit('listaPersona', usuarios.getPersonasPorSala(data.sala))
    callback( usuarios.getPersonasPorSala(data.sala))
});

client.on('crearMensaje', (data, callback)=>{
    let nombre = usuarios.getPersona(client.id)
    
    let mensaje = crearMensaje(nombre.nombre, data.mensaje);
    client.broadcast.to(nombre.sala).emit('crearMensaje', mensaje)
    callback(mensaje)
})

    client.on('disconnect',()=>{
        
        let persona = usuarios.borrarPersona(client.id);
        
            client.broadcast.to(persona.sala).emit('crearMensaje',crearMensaje('Administrador',`${persona.nombre} salió`))
            client.broadcast.to(persona.sala).emit('listaPersona', usuarios.getPersonasPorSala(persona.sala))
     
        
       
    
})
//Mensaje privados

client.on('mensajePrivado', (data)=>{
    let persona = usuarios.getPersona( client.id)
    client.broadcast.to(data.para).emit('mensajePrivado', crearMensaje(persona.nombre, data.mensaje))
})



    
})



